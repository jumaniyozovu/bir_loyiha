const Category = require('../model/Category')
const ErrorHandler = require('../utils/ErrorHandler')

//@desc     Get All Category
//@route    /api/category/all
//@type     Public
//@role     User
exports.getAllCategory = async (req,res,next)=>{
    const category = await Category.find()
    if(category){
        res.status(200).json({
            success: true,
            data: category
        })
    } else {
        res.status(404).json({
            success: true,
            data: 'Category Not Found'
        })
    }
}

//@desc     Add Category
//@route    /api/category/add
//@type     Public
//@role     User
exports.addCategory = (req,res,next) =>{
    if(req.body){
    const category = new Category({
        title: req.body.title
    })
        category.save()
            .then(()=> {
                res.status(201).json({
                    success: true,
                    category
                })
            }) .catch((e)=> {
                return res.send(new ErrorHandler(e,400))
        })
    } else {
        res.status(400).json({
            success: false,
            data: "Ma`lumotlar bo'sh. Iltimos ma'lumotlarni to'ldiring"
        })
    }
}

//@desc     Delete Category
//@route    /api/category/:id
//@type     Public
//@role     User
exports.deleteCategory = async (req,res,next) =>{
    const cateogry = await Category.findByIdAndDelete(req.params.id)
    if(cateogry){
        res.status(200).json({
            success: true,
            data: {}
        })
    } else {
        res.status(404).json({
            success: false,
            data: 'Category Not Found'
        })
    }
}
