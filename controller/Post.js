const Post = require('../model/Post')
const ErrorResponse = require('../utils/ErrorHandler')

// @desc      Get All post
// @route     GET /post/all
// @access    Public
exports.getAllPosts = async (req,res,next) => {
    const posts = await Post.find().sort({date: -1})
    res.send({
        success: true,
        data: posts
    })
}
// @desc      Add  product
// @route     POST /post/add
// @access    Public
exports.addPost = async (req,res,next) => {
    const post = new Post({
        title: req.body.title,
        description: req.body.description,
        category: req.body.category,
        image: req.body.image
    })
    post.save().then(()=>{
        res.status(201).json({
            success: true,
            data: post
        })
    })
        .catch((e) => {
            res.status(400).json({
                success: false,
                error: e
            })
        })
}

// @desc      Post By Id
// @route     GET /post/:id
// @access    Public
exports.getById = async (req,res,next) => {
    const post = await Post.findById({_id: req.params.id})
    if(post){
        res.send({
            success: true,
            data: post
        })
    } else {
        res.send({
            error: new ErrorResponse('Post not found', 404)
        })
    }
}
// @desc      Post By Id
// @route     GET /post/:id
// @access    Public
exports.deleteById = async (req,res,next) => {
    const post = await Post.findByIdAndDelete(req.params.id)
    if(post){
        res.status(200).json({
            success: true,
            data: {}
        })
    } else {
        res.status(404).json({
            success: true,
            data: 'Post Not Found'
        })
    }
}

// @desc      Post By Id
// @route     GET /post/:id
// @access    Public
exports.getByCategory = async (req,res,next) => {
    const posts = await Post.find({category:req.params.categoryId})

    if(posts){
        res.status(200).json({
            success: true,
            data: posts
        })
    } else {
        res.status(404).json({
            success: true,
            data: {}
        })
    }
}
