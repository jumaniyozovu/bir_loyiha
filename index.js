const express = require('express')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser')
const Post = require('./routes/post')
const Category = require('./routes/category')
const connectDB = require('./config/db');

connectDB();

app.use(bodyParser.json())
app.use(cors({ rogin : "*" }));
app.get('/',(req,res)=>{
    res.send("Hello Server")
})

app.use('/post',Post)
app.use('/category',Category)

const PORT = 3200
app.listen(PORT, ()=>{
    console.log(`Server running this ${PORT}`)
})
