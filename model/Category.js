const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({
    title: {type: String, reuqired: true, unique: true},
    date: {type: Date, default: Date.now()}
})

module.exports = mongoose.model('Category', CategorySchema)
