const mongoose = require('mongoose')

const PostSchema = new mongoose.Schema({
    title: {required: true, type: String},
    category: {
        required: true,
        type: mongoose.Schema.ObjectId,
        ref: 'Category'
    },
    image: {required: true, type: String},
    description: {required: true, type: String},
    date: {type: Date, default: Date.now()}
})

module.exports = mongoose.model('News', PostSchema)
