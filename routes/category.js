const express = require('express')
const router = express.Router()
const {
    addCategory,
    deleteCategory,
    getAllCategory
} = require('../controller/Category')

router.post('/add' , addCategory)
router.get('/all', getAllCategory)
router.delete('/:id', deleteCategory)

module.exports = router
