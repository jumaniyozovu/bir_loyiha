const express = require('express')

const {
    getById,
    addPost,
    getAllPosts,
    deleteById,
    getByCategory
} = require('../controller/Post')

const router = express.Router()
router.post('/add', addPost)
router.get('/all', getAllPosts)

router.get('/:id', getById)
router.get('/category/:categoryId', getByCategory)
router.delete('/:id', deleteById)

module.exports = router
