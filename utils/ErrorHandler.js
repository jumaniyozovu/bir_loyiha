class ErrorResponse extends Error {
    constructor(message, statusCode) {
        super(message);
        this.statusCode = statusCode;
        ErrorResponse.captureStackTrace(this, this.constructor)
    }

}

module.exports = ErrorResponse
